#ifndef SEASONMANAGER_H
#define SEASONMANAGER_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QBasicTimer>
#include <QObject>

class SeasonManager : public QObject
{
    Q_OBJECT
public:
    SeasonManager(int* seasons, float time_per_season=600.0f, int actual_time=0,int fps=60);
    void print_season(){fprintf(stderr,"%d\n",seasons[0]);}

protected:
    void timerEvent(QTimerEvent *e) override;

private :
    QBasicTimer timer;
    float time_per_season;
    int actual_time;
    int fps;
    int seasons[4] = {1,2,3,4};
    void setSeason(int widget,int newSeason);


signals:
    void seasonChange(int* seasons);
};

#endif // SEASONMANAGER_H
