#include "seasonmanager.h"

SeasonManager::SeasonManager(int* seasons,float time_per_season,int actual_time,int fps)
{
    this->time_per_season=time_per_season;
    this->actual_time = actual_time;
    this->fps = fps;
    for(int i=0;i<4;i++){
        this->seasons[i]= seasons[i];
    }
    timer.start(1000/fps, this);
}

void SeasonManager::timerEvent(QTimerEvent *e)
{
    actual_time++;
    if(actual_time>time_per_season){
        for(int i=0;i<4;i++){
            this->seasons[i]=(this->seasons[i]+1)%4;
        }
        emit(seasonChange(this->seasons));
        actual_time=0;
    }
}
