QT       += core gui widgets

TARGET = cube
TEMPLATE = app

SOURCES += main.cpp \
    seasonmanager.cpp \
    mainwidget.cpp \
    geometryengine.cpp \
    quadtree.cpp

HEADERS += \
    mainwidget.h \
    geometryengine.h \
    seasonmanager.h \
    quadtree.h \
    vertexdata.h

RESOURCES += \
    shaders.qrc \
    textures.qrc

# install
target.path = $$[QT_INSTALL_EXAMPLES]/opengl/cube
INSTALLS += target
